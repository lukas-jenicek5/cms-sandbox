<?php

namespace App\BackendModule\Presenters;

use Nette;
use WebLoader\Nette\LoaderFactory;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/**
	 * @var LoaderFactory
	 * @inject
	 */
	public $webloader;

	protected function createComponentWebloaderCss() {
		return $this->webloader->createCssLoader('backend');
	}


	protected function createComponentWebloaderJs() {
		return $this->webloader->createJavaScriptLoader('backend');
	}


    public function startup() {
        parent::startup();
        if($this->user->isLoggedIn() == null && $this->presenter instanceof SignPresenter == false) {
            $this->redirect('Sign:in');
        }
    }

}
