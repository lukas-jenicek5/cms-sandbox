<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public static function createRouter()
	{
        $router = new RouteList();

        /** ADMIN MODULE */
        $router[] = new Route('admin/<presenter>/<action>[/<id>]', array(
            'module' => 'Backend',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL
        ));

        /** FRONTMODULE */
        $router[] = new Route('<presenter>/<action>[/<id>]', array(
            'module' => 'Frontend',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL
        ));

        return $router;
	}

}
