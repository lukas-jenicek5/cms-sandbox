<?php

namespace Kdyby\Doctrine\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * IdentifiedEntity
 *
 * @ORM\Table(name="identified_entity")
 * @ORM\MappedSuperClass
 */
class IdentifiedEntity extends \Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
