<?php

namespace Kdyby\Doctrine\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseEntity
 *
 * @ORM\Table(name="base_entity")
 * @ORM\MappedSuperClass
 */
class BaseEntity extends \Nette\Object
{

}
