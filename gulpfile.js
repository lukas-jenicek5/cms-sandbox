var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css'),
    notify = require('gulp-notify'),
    compass = require('gulp-compass'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');


var jsBaseDir = __dirname + '/www/clientside/js';

var config = {
    src: {
        sass: __dirname + '/www/clientside/sass'
    },
    dest: {
        js: __dirname + '/www/build/js/',
        css: __dirname + '/www/build/css/'
    }
};

var jsLibs = [
    jsBaseDir + '/jquery/dist/jquery.min.js',
    jsBaseDir + '/bootstrap/dist/js/bootstrap.min.js',
    jsBaseDir + '/nette.ajax.js/nette.ajax.js',
    jsBaseDir + '/nette-forms/src/assets/netteForms.js',
    jsBaseDir + '/main.js'
];

gulp.task('build', function() {

    gulp.src(jsLibs)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.dest.js))

    gulp.src(__dirname + '/www/clientside/sass/screen.scss')
        .pipe(sass())
        .pipe(concat('screen.css'))
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest.css))

});


gulp.task('js', function() {
    gulp.src(jsLibs)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.dest.js))
});

gulp.task('sass', function() {
    gulp.src(__dirname + '/www/clientside/sass/screen.scss')
        .pipe(sass())
        .pipe(concat('screen.css'))
        .pipe(minifycss())
        .pipe(gulp.dest(config.dest.css))

});