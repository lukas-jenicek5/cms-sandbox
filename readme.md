Nette Sandbox
=============

Sandbox is a pre-packaged and pre-configured Nette Framework application
that you can use as the skeleton for your new applications.

[Nette](http://nette.org) is a popular tool for PHP web development.
It is designed to be the most usable and friendliest as possible. It focuses
on security and performance and is definitely one of the safest PHP frameworks.


Installing
----------

The best way to install Sandbox is using Composer. If you don't have Composer yet, download
it following [the instructions](http://doc.nette.org/composer). Then use command:

		composer create-project -s dev lukascms/sandbox jmeno-projektu
		mkdir temp && mkdir log
        chmod -R 775 temp && chown -R user:www-data jmeno-projektu
        chmod -R 775 log
        bower update
        gulp build


License
-------
- Nette: New BSD License or GPL 2.0 or 3.0 (http://nette.org/license)
- Adminer: Apache License 2.0 or GPL 2 (http://www.adminer.org)
